using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Valve.VR.InteractionSystem.Sample
{
    public class GravityGun : MonoBehaviour
    {
        public Hand hand;
        [SerializeField] private LineRenderer line;
        [SerializeField] private Collider checkCollider;
        private Vector3 pointRay;
        private bool keepSmth;
        private Transform keepObject;
        private Transform lastObject;

        private void OnEnable()
        {
            keepSmth = false;
            if (hand == null)
                hand = this.GetComponent<Hand>();
        }
        private void Update()
        {
            if (SteamVR_Actions.inventory_GravityGunTake[hand.handType].state && !keepSmth)
            {
                checkCollider.enabled = true;
                line.enabled = true;
                line.SetPosition(1, transform.position);
                DrawRay();
                line.SetPosition(0, pointRay);
            }
            else if (SteamVR_Actions.default_GrabGrip[hand.handType].state && keepSmth) Lay();
            else
            {
                checkCollider.enabled = false;
                line.enabled = false;
            }
            if (SteamVR_Actions.inventory_GravityGunThrow[hand.handType].state && keepSmth) Throw();
        }

        private void DrawRay()
        {
            Ray ray = new Ray(transform.position, transform.forward * 100f);
            if (Physics.Raycast(ray, out RaycastHit hitInfo))
            {
                pointRay = hitInfo.point;
                if (hitInfo.transform.TryGetComponent(out Throwable throwable))
                {
                    lastObject = hitInfo.transform;
                    hitInfo.rigidbody.velocity = Vector3.zero;
                    hitInfo.rigidbody.angularVelocity = Vector3.zero;
                    hitInfo.rigidbody.useGravity = false;
                    hitInfo.transform.position = Vector3.MoveTowards(hitInfo.transform.position, transform.position, Time.deltaTime * 2f);
                }
                else if (lastObject != null) lastObject.GetComponent<Rigidbody>().useGravity = true;
            }
            else
            {
                if(lastObject != null) lastObject.GetComponent<Rigidbody>().useGravity = true;
                pointRay = transform.position + transform.forward * 100f;
            }

        }
        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Object")
            {
                keepSmth = true;
                keepObject = other.transform;
                keepObject.SetParent(transform);
                keepObject.GetComponent<Collider>().enabled = false;
            }
        }

        private void Throw()
        {
            keepObject.SetParent(transform.parent.parent.parent);
            keepObject.GetComponent<Collider>().enabled = true;
            keepObject.GetComponent<Rigidbody>().useGravity = true;
            keepObject.GetComponent<Rigidbody>().velocity = transform.forward * 8f;
            keepSmth = false;
            keepObject = null;
        }

        private void Lay()
        {
            keepObject.SetParent(transform.parent.parent.parent);
            keepObject.GetComponent<Collider>().enabled = true;
            keepObject.GetComponent<Rigidbody>().useGravity = true;
            keepSmth = false;
            keepObject = null;
        }
    }
}