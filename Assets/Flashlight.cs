//======= Copyright (c) Valve Corporation, All rights reserved. ===============

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

namespace Valve.VR.InteractionSystem.Sample
{
    public class Flashlight : MonoBehaviour
    {
        public Hand hand;
        public GameObject flashlight;
        public GameObject directLight;

        private void OnEnable()
        {
            if (hand == null)
                hand = this.GetComponent<Hand>();
        }
        private void Update()
        {
            if (SteamVR_Actions.inventory.Flashlight[hand.handType].state)
                flashlight.SetActive(true);
            else flashlight.SetActive(false);
            if (SteamVR_Actions.inventory.directLight[hand.handType].state)
                directLight.SetActive(true);
            else directLight.SetActive(false);
        }
    }
}